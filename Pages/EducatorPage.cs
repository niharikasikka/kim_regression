﻿using KIM_SmokeTest.BaseClass;
using KIM_SmokeTest.UtilityClasses;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using TechTalk.SpecFlow;

namespace KIM_Automation.Pages
{
    class EducatorPage : BaseApplicationPage
    {
        public EducatorPage(IWebDriver driver) : base(driver)
        { }
        //Educator Elements
        public By AddOtherEducator_Element => By.XPath("//strong[contains(text(),'Other Educators')]");
        public By AddNewOtherEducator_Element => By.XPath("//input[@class='btn blue add-assistant']");
        public By GivenName_Educator => By.XPath("//form[@id='assistantAddEditForm']//input[@id='assDetailsGName']");
        public By FamilyName_Educator => By.CssSelector("#assDetailsFName");
        public By Gender_Educator => By.CssSelector("#assOptGender");
        public By TeachingQualification_Educator => By.CssSelector("#assQualification");
        public By IndustrialAgreement_Educator => By.CssSelector("#assAward");
        public By TeacherLevel_Educator => By.CssSelector("#assLevel");
        //Save Btn
        By SaveBtn_Educator => By.XPath("//div[@id='assistantAddEditModal']//div[@class='modal-dialog']//form[@id='assistantAddEditForm']//input[@name='btnSave']");
        public By DOB_Educator => By.CssSelector("#assDetailsDOB");
        public By ProfDevHours_Educator => By.XPath("//form[@id='assistantAddEditForm']//div[@class='modal-body clearfix']//input[@id='assNoProDevHrs']");
        public By EmpHours_Educator => By.CssSelector("#assKHrs4Yr");
        public IWebElement Hoursworkedin_Educator => Driver.FindElement(By.XPath("//input[@id='assNKHrs4Yr']"));
        public By SearchEducator_Element => By.XPath("//div[@id='DataTables_Table_1_filter']//input");
        public By EditEducator_Element => By.XPath("//body[contains(@class,'ms-backgroundImage')]/form[@id='aspnetForm']/div[@id='s4-workspace']/div[@id='s4-bodyContainer']/div/div[contains(@class,'container')]/div[@id='MSO_ContentTable']/div/span[@id='DeltaPlaceHolderMain']/div[contains(@class,'profile')]/div[@id='ctl00_PlaceHolderMain_fundedServicePanel']/div[contains(@class,'tabs datacollectiontabs')]/div[@id='profileTabsContent']/div[@id='tab2']/div[@id='DataTables_Table_1_wrapper']/table[@id='DataTables_Table_1']/tbody[@id='CoEducatorSummaryDataGrid']/tr[1]/td[3]/span[1]/button[1]");

        public By DeleteEducator_Element => By.XPath("//td[contains(@class,'sorting_3')]//button[contains(@class,'btn blue delete')][contains(text(),'Remove')]");
        public By FinalDate_ElementIcon => By.XPath("//form[@id='assistantDeleteForm']//span[@class='input-group-addon']");
        public IWebElement FinalDate_Select => Driver.FindElement(By.XPath("//td[contains(@class,'today active day')]"));
        public IWebElement Dropdown_Element => Driver.FindElement(By.CssSelector("#assReasonLeaving"));
        public IWebElement RemoveButton_Educator => Driver.FindElement(By.XPath("//form[@id='assistantDeleteForm']//input[contains(@class,'btn blue btn-primary delete submit')]"));
        public By Element => By.XPath("//div[@id='tab2']//tr[1]//td[2]");
        public By OtherEducatorsTabNoofInactiveEducatorElement => By.XPath("//div[@id='DataTables_Table_1_info']");
        public By ShowInactiveEducatorButton_Teacher_Element => (By.XPath("//button[@class='btn blue assistant-active-toggle pull-left active-state']"));
        public By ShowActiveEducatorButton_Teacher_Element => (By.XPath("//button[@class='btn blue assistant-active-toggle pull-left inactive-state']"));
        internal void FillEducatorForm(Table table)
        {
            var dataTable = TableExtensions.ToDataTable(table);
            foreach (DataRow row in dataTable.Rows)
            {
                Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(50);

                //wait.Until(x => x.FindElement(GivenName_Educator));
                //Driver.FindElement(GivenName_Educator).Click();
                Driver.FindElement(GivenName_Educator).SendKeys(row.ItemArray[0].ToString());
                //WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
                //wait.Until(x => x.FindElement(FamilyName_Educator));
                Driver.FindElement(FamilyName_Educator).SendKeys(row.ItemArray[1].ToString());
                //Wait for gender dropdown to be visible and then select the value
                //wait.Until(x => x.FindElement(Gender_Educator));
                //Selecting a Gender type from dropdown
                SelectElement Gender = new SelectElement(Driver.FindElement(Gender_Educator));
                Gender.SelectByText(row.ItemArray[2].ToString());
                //wait.Until(x => x.FindElement(DOB_Educator));
                Driver.FindElement(DOB_Educator).SendKeys(row.ItemArray[3].ToString());
                // Selecting the TeacherQualification from dropdown
                SelectElement TeachingQualification = new SelectElement(Driver.FindElement(TeachingQualification_Educator));
                TeachingQualification.SelectByText(row.ItemArray[4].ToString());
                // Selecting the Industrial Agreement from dropdown
                SelectElement IndustrialAgreement = new SelectElement(Driver.FindElement(IndustrialAgreement_Educator));
                IndustrialAgreement.SelectByText(row.ItemArray[5].ToString());
                // Selecting Teacher Level dropdown
                SelectElement Level = new SelectElement(Driver.FindElement(TeacherLevel_Educator));
                Level.SelectByText(row.ItemArray[6].ToString());
                //Entering hours of employment
                //wait.Until(x => x.FindElement(ProfDevHours_Educator));
                Driver.FindElement(ProfDevHours_Educator).SendKeys(row.ItemArray[7].ToString());
                //wait.Until(x => x.FindElement(EmpHours_Educator));
                Driver.FindElement(EmpHours_Educator).SendKeys(row.ItemArray[8].ToString());
                Hoursworkedin_Educator.SendKeys(row.ItemArray[9].ToString());
                //Thread.Sleep(5000);
                //wait.Until(x => x.FindElement(SaveBtn_Educator));
                Driver.FindElement(SaveBtn_Educator).Click();
                Thread.Sleep(2000);
                AddOtherEducators();
            }
        }
        internal void AddOtherEducators()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(AddOtherEducator_Element));
            Driver.FindElement(AddOtherEducator_Element).Click();
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(50);
            wait.Until(x => x.FindElement(AddNewOtherEducator_Element));
            Driver.FindElement(AddNewOtherEducator_Element).Click();
        }

        internal void EditEducator(string educator)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(AddOtherEducator_Element));
            Driver.FindElement(AddOtherEducator_Element).Click();
            wait.Until(x => x.FindElement(SearchEducator_Element));
            Driver.FindElement(SearchEducator_Element).Click();
            Driver.FindElement(SearchEducator_Element).SendKeys(educator);
            //Clicking on edit button
            wait.Until(x => x.FindElement(EditEducator_Element));
            Driver.FindElement(EditEducator_Element).Click();
        }

        internal void EditDob(string dob)
        {
            //Updating the date of birth of the educator
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(DOB_Educator));
            //Clear the dob field
            Thread.Sleep(2000);
            Driver.FindElement(DOB_Educator).Clear();
            Driver.FindElement(DOB_Educator).SendKeys(dob);
        }

        internal void EditHoursEmployment(string hoursEmployment)
        {
            //Updating the hours of employment
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(EmpHours_Educator));
            //Clear the dob field
            Driver.FindElement(EmpHours_Educator).Clear();
            Driver.FindElement(EmpHours_Educator).SendKeys(hoursEmployment);
            wait.Until(x => x.FindElement(SaveBtn_Educator));
            Driver.FindElement(SaveBtn_Educator).Click();
            Thread.Sleep(2000);
        }

        internal void DeleteEducator(string name)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(AddOtherEducator_Element));
            Driver.FindElement(AddOtherEducator_Element).Click();
            wait.Until(x => x.FindElement(SearchEducator_Element));
            Driver.FindElement(SearchEducator_Element).Click();
            Driver.FindElement(SearchEducator_Element).SendKeys(name);

            wait.Until(x => x.FindElement(DeleteEducator_Element));
            Driver.FindElement(DeleteEducator_Element).Click();
        }
        internal void ConfirmRemove_Educator(string reason)
        {
            Thread.Sleep(3000);
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(FinalDate_ElementIcon));
            //Select final date from date picker
            Driver.FindElement(FinalDate_ElementIcon).Click();
            FinalDate_Select.Click();
            //Select reason for leaving from dropdown
            var selectElement_reason = new SelectElement(Dropdown_Element);
            selectElement_reason.SelectByText(reason);
            RemoveButton_Educator.Click();
            Thread.Sleep(2000);
        }
        internal void VerifyDeleteEducator(string name)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(SearchEducator_Element));
            Driver.FindElement(SearchEducator_Element).Click();
            Driver.FindElement(SearchEducator_Element).SendKeys(name);
            bool a = CommonFunctions.IsElementDisplayed(Driver, Element);
            string b = a.ToString();
            Assert.AreEqual(b, "False");
        }

        internal void CompleteEducatorData(string hours)
        {

            //WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            //wait.Until(x => x.FindElement(AddOtherEducator_Element));
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(50);
            Driver.FindElement(AddOtherEducator_Element).Click();
            //number of rows in educator table 
            IList<IWebElement> TableRows = Driver.FindElements(By.XPath("//tbody[@id='CoEducatorSummaryDataGrid']//tr"));
            for (int i = 1; i <= TableRows.Count; i++)
            {
                Driver.FindElement(By.XPath("//tbody[@id='CoEducatorSummaryDataGrid']//tr[" + i + "]//button[contains(@class,'btn blue edit')][contains(text(),'Edit')]")).Click();
                //Entering hours of employment
                //wait.Until(x => x.FindElement(ProfDevHours_Educator));
                Driver.FindElement(ProfDevHours_Educator).Click();
                Driver.FindElement(ProfDevHours_Educator).Clear();
                Driver.FindElement(ProfDevHours_Educator).SendKeys(hours);
                Hoursworkedin_Educator.Click();
                Hoursworkedin_Educator.Clear();
                Hoursworkedin_Educator.SendKeys(hours);
                //wait.Until(x => x.FindElement(SaveBtn_Educator));
                Thread.Sleep(2000);
                Driver.FindElement(SaveBtn_Educator).Click();
                Thread.Sleep(2000);
            }
        }
        internal void MakeInactiveEducatorActive()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));

            wait.Until(x => x.FindElement(AddOtherEducator_Element));
            Driver.FindElement(AddOtherEducator_Element).Click();

            wait.Until(x => x.FindElement(ShowInactiveEducatorButton_Teacher_Element));
            Driver.FindElement(ShowInactiveEducatorButton_Teacher_Element).Click();
            char[] charsToTrim = { 'e', 'n', 't', 'r', 'i', 's' };
            String strTeachersTabUntrimmedNoofInactiveEducators = Driver.FindElement(OtherEducatorsTabNoofInactiveEducatorElement).Text;
            String strFirstTrimNoofInactiveEducator = strTeachersTabUntrimmedNoofInactiveEducators.TrimEnd(charsToTrim);
            String strSecondTrimNoofInactiveEducator = strFirstTrimNoofInactiveEducator.Substring(strFirstTrimNoofInactiveEducator.IndexOf("of") + 2);
            String strNoofInactiveEducator = strSecondTrimNoofInactiveEducator.Trim();
            if (strNoofInactiveEducator.Equals("0"))
            {

            }
            else
            {
                IList<IWebElement> TableRows = Driver.FindElements(By.XPath("//tbody[@id='CoEducatorSummaryDataGrid']//tr"));
                for (int i = 1; i <= TableRows.Count; i++)
                {

                    //wait.IgnoreExceptionTypes();
                    wait.Until(x => x.FindElement(By.XPath("//tbody[@id='CoEducatorSummaryDataGrid']//tr[1]//button[@class='btn blue activate']")).Displayed);
                    Driver.FindElement(By.XPath("//tbody[@id='CoEducatorSummaryDataGrid']//tr[1]//button[@class='btn blue activate']")).Click();
                    Thread.Sleep(2000);
                }
            };
            wait.Until(x => x.FindElement(ShowActiveEducatorButton_Teacher_Element));
            Driver.FindElement(ShowActiveEducatorButton_Teacher_Element).Click();
        }
        public int NoofEducator()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(160));
            wait.Until(x => x.FindElement(By.XPath("//strong[contains(text(),'Other Educators')]")));
            Driver.FindElement(By.XPath("//strong[contains(text(),'Other Educators')]")).Click();

            Thread.Sleep(2000);
            char[] charsToTrim = { 'e', 'n', 't', 'r', 'i', 's' };
            String strOtherEducatorsTabUntrimmedNoofTeachers = Driver.FindElement(By.CssSelector("#DataTables_Table_1_info")).Text;
            String strFirstTrimNoofEducators = strOtherEducatorsTabUntrimmedNoofTeachers.TrimEnd(charsToTrim);
            String strSecondTrimNoofEducators = strFirstTrimNoofEducators.Substring(strFirstTrimNoofEducators.IndexOf("of") + 2);
            String strNoofEducators = strSecondTrimNoofEducators.Trim();

            int noofEducator = Convert.ToInt32(strNoofEducators);
            return noofEducator;
        }
    }
}
