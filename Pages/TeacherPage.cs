﻿using KIM_SmokeTest.BaseClass;
using KIM_SmokeTest.UtilityClasses;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using TechTalk.SpecFlow;

namespace KIM_Automation.Pages
{
    class TeacherPage : BaseApplicationPage
    {
        public TeacherPage(IWebDriver driver) : base(driver)
        { }

        public By AddNewTeacher_Element => By.XPath("//strong[contains(text(),'Teachers')]");
        public By AddNewTeacherLookUp_Element => By.XPath("//input[@class='btn blue add-teacher-lookup']");
        public By GivenName_Element => By.CssSelector("#txtDetailsGName");
        //Selecting a Gender type from dropdown
        public By Gender_dropdown => By.CssSelector("#ddlOptGender");
        // Selecting the Registration Status from dropdown
        By TeachersRegStatus_dropdown => By.CssSelector("#ddlOptVITRegStatus");
        //Selecting the radio button
        IWebElement Qualification_radiobtn => Driver.FindElement(By.CssSelector("#rbOptTeacherQualsApplied1"));
        IWebElement QualificationDetails_radiobtn => Driver.FindElement(By.CssSelector("#opt3or4Year2"));
        //Selecting the check box
        By Qualification_checkbox => By.CssSelector("#chkAusQualificationHardCopy");
        //University Dropdown
        IWebElement University_dropdown => Driver.FindElement(By.CssSelector("#ddlLstUniversity"));
        //Course dropdown 
        IWebElement Course_dropdown => Driver.FindElement(By.CssSelector("#ddlLstCourse"));
        //Next btn
        By BtnNext => By.XPath("//form[@id='teacherAddEditForm']//button[@name='btnNext'][contains(text(),'Next')]");
        //Teachershare dropdown
        By Teachershare_dropdown => By.CssSelector("#ddlOptShareGrpTeaching");
        //Award agreement dropdown
        IWebElement Awardagreement_dropdown => Driver.FindElement(By.CssSelector("#ddlLstAward"));
        // Teacher Level dropdown
        IWebElement TeacherLevel_dropdown => Driver.FindElement(By.CssSelector("#ddlLstLevel"));
        //Save Btn
        By SaveBtn => By.XPath("//form[@id='teacherAddEditForm']//input[@name='btnSave']");
        public IWebElement FamilyName_Element => Driver.FindElement(By.CssSelector("#txtDetailsFName"));
        public IWebElement DOB_Element => Driver.FindElement(By.CssSelector("#txtDetailsDOB"));
        public By CommencementData_Element => By.CssSelector("#txtDateCommenced");
        public By VITNum_Element => By.CssSelector("#txtVITNumber");
        public By NumGroups_Element => By.CssSelector("#txtFundedGroups");
        public IWebElement YearAwarded_Element => Driver.FindElement(By.CssSelector("#intAusYearAwarded"));
        public By NumHours_Element => By.CssSelector("#txtPDHrs");
        public By DeleteTeacher_Element => By.XPath("//tr[1]//td[5]//span[1]//button[2]");
        public By SearchTeacher_Element => By.XPath("//div[@id='DataTables_Table_0_filter']//input");
        public By FinalDate_ElementIcon => By.CssSelector("#dtDepartureDate");
        public IWebElement Dropdown_Element => Driver.FindElement(By.CssSelector("#ddlReasonLeaving"));
        public IWebElement FinalDate_Select => Driver.FindElement(By.XPath("//td[@class='today active day']"));
        public IWebElement RemoveButton_Teacher => Driver.FindElement(By.XPath("//form[@id='teacherDeleteForm']//input[contains(@class,'btn blue btn-primary delete submit')]"));

        public By EditTeacher_Element => By.XPath("//tbody[@id='TeacherSummaryDataGrid']//button[@class='btn blue edit'][contains(text(),'Edit')]");
        public By TeachersTab_Element => By.XPath("//strong[contains(text(),'Teachers')]");
        public By TeachersTabNoofInactiveTeacherElement => By.XPath("//div[@id='DataTables_Table_0_info']");
        public By ShowInactiveTeacherButton_Teacher_Element => (By.XPath("//button[@class='btn blue teacher-active-toggle pull-left active-state']"));
        public By ShowActiveTeacherButton_Teacher_Element => (By.XPath("//button[@class='btn blue teacher-active-toggle pull-left inactive-state']"));
        public By AddNewTeacher_Button => By.XPath("//input[@id='add-teacher-lookup']");

        internal void FillTeacherForm(Table table)
        {
            var dataTable = TableExtensions.ToDataTable(table);
            foreach (DataRow row in dataTable.Rows)
            {

                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
                wait.Until(x => x.FindElement(GivenName_Element));
                Driver.FindElement(GivenName_Element).SendKeys(row.ItemArray[0].ToString());
                FamilyName_Element.SendKeys(row.ItemArray[1].ToString());
                //Wait for gender dropdown to be visible and then select the value
                wait.Until(x => x.FindElement(Gender_dropdown));
                SelectElement Gender = new SelectElement(Driver.FindElement(Gender_dropdown));
                Gender.SelectByText(row.ItemArray[2].ToString());
                DOB_Element.SendKeys(row.ItemArray[3].ToString());
                //Waiting for the element to be visible and entering commencedate
                Thread.Sleep(2000);
                wait.Until(x => x.FindElement(CommencementData_Element));
                Driver.FindElement(CommencementData_Element).SendKeys(row.ItemArray[4].ToString());
                SelectElement RegStatus = new SelectElement(Driver.FindElement(TeachersRegStatus_dropdown));
                RegStatus.SelectByText(row.ItemArray[5].ToString());
                Driver.FindElement(VITNum_Element).SendKeys(row.ItemArray[6].ToString());
                Qualification_radiobtn.Click();
                SelectElement University = new SelectElement(University_dropdown);
                University.SelectByText(row.ItemArray[7].ToString());
                SelectElement Course = new SelectElement(Course_dropdown);
                Course.SelectByText(row.ItemArray[8].ToString());
                YearAwarded_Element.SendKeys(row.ItemArray[9].ToString());
                Driver.FindElement(Qualification_checkbox).Click();
                QualificationDetails_radiobtn.Click();
                wait.Until(x => x.FindElement(BtnNext));
                //Waiting for the element to be visible and clicking on next button
                Driver.FindElement(BtnNext).Click();
                Driver.FindElement(NumGroups_Element).SendKeys(row.ItemArray[10].ToString());
                SelectElement Teachershare = new SelectElement(Driver.FindElement(Teachershare_dropdown));
                Teachershare.SelectByText(row.ItemArray[11].ToString());
                SelectElement Awardagreement = new SelectElement(Awardagreement_dropdown);
                Awardagreement.SelectByText(row.ItemArray[12].ToString());
                SelectElement TeacherLevel = new SelectElement(TeacherLevel_dropdown);
                TeacherLevel.SelectByText(row.ItemArray[13].ToString());
                wait.Until(x => x.FindElement(NumHours_Element));
                Driver.FindElement(NumHours_Element).SendKeys(row.ItemArray[14].ToString());
                wait.Until(x => x.FindElement(SaveBtn));
                Thread.Sleep(2000);
                Driver.FindElement(SaveBtn).Click();
                Thread.Sleep(2000);
                AddNewTeacher();
            }
        }
        internal void AddNewTeacher()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(AddNewTeacher_Element));
            Driver.FindElement(AddNewTeacher_Element).Click();
            Thread.Sleep(3000);
            wait.Until(x => x.FindElement(AddNewTeacherLookUp_Element));
            Driver.FindElement(AddNewTeacherLookUp_Element).Click();
            Thread.Sleep(3000);
            wait.Until(x => x.FindElement(AddNewTeacher_Button));
            Driver.FindElement(AddNewTeacher_Button).Click();
        }

        internal void DeleteTeacher(string teacher)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(SearchTeacher_Element));
            Driver.FindElement(SearchTeacher_Element).Click();
            Driver.FindElement(SearchTeacher_Element).SendKeys(teacher);

            wait.Until(x => x.FindElement(DeleteTeacher_Element));
            Driver.FindElement(DeleteTeacher_Element).Click();

        }

        internal void ConfirmRemove_Teacher(string reason)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(FinalDate_ElementIcon));
            //Select final date from date picker
            Driver.FindElement(FinalDate_ElementIcon).Click();
            FinalDate_Select.Click();
            //Select reason for leaving from dropdown
            var selectElement_reason = new SelectElement(Dropdown_Element);
            selectElement_reason.SelectByText(reason);
            RemoveButton_Teacher.Click();
            Thread.Sleep(2000);
        }

        internal void VerifyDelete_Teacher(string teacher)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(SearchTeacher_Element));
            Driver.FindElement(SearchTeacher_Element).Click();
            Driver.FindElement(SearchTeacher_Element).SendKeys(teacher);
            //Assert.IsFalse(Driver.FindElement(By.XPath("//td[contains(text()," + teacher + ")])")).Displayed);
        }

        internal void EditTeacher(string teacher)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(SearchTeacher_Element));
            Driver.FindElement(SearchTeacher_Element).Click();
            Driver.FindElement(SearchTeacher_Element).SendKeys(teacher);
            //Clicking on edit button
            wait.Until(x => x.FindElement(EditTeacher_Element));
            Driver.FindElement(EditTeacher_Element).Click();
        }

        internal void EditCommenceDate(string commenceDate)
        {
            //Updating the commence date of the teacher
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(CommencementData_Element));
            Driver.FindElement(CommencementData_Element).SendKeys(commenceDate);
        }

        internal void EditGroupNum(string groupNum)
        {
            Thread.Sleep(3000);
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(BtnNext));
            // CommonFunctions.ExplicitWait(Driver, BtnNext);
            Driver.FindElement(BtnNext).Click();
            wait.Until(x => x.FindElement(NumHours_Element));
            CommonFunctions.EnterText(Driver.FindElement(NumGroups_Element), groupNum);
            //NumGroups_Element.SendKeys(groupNum);
            wait.Until(x => x.FindElement(SaveBtn));
            //CommonFunctions.ExplicitWait(Driver, SaveBtn);
            Driver.FindElement(SaveBtn).Click();

        }

        internal void CompleteTeacherData(Table table)
        {
            IList<IWebElement> TableRows = Driver.FindElements(By.XPath("//tbody[@id='TeacherSummaryDataGrid']//tr"));

            for (int i = 1; i <= TableRows.Count; i++)
            {

                Driver.FindElement(By.XPath("//tbody[@id='TeacherSummaryDataGrid']//tr[" + i + "]//button[contains(@class,'btn blue edit')][contains(text(),'Edit')]")).Click();
                var dataTable = TableExtensions.ToDataTable(table);
                foreach (DataRow row in dataTable.Rows)
                {
                    // do something with TableRows.ElementAt(i);
                    WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(40));
                    //wait.Until(x => x.FindElement(TeachersRegStatus_dropdown));
                    //SelectElement RegStatus = new SelectElement(Driver.FindElement(TeachersRegStatus_dropdown));
                    //RegStatus.SelectByText(row.ItemArray[0].ToString());
                    //wait.Until(x => x.FindElement(VITNum_Element));
                    //Driver.FindElement(VITNum_Element).SendKeys(row.ItemArray[1].ToString() + i.ToString());
                    //Thread.Sleep(2000);
                    //wait.Until(x => x.FindElement(Qualification_checkbox));
                    //Driver.FindElement(Qualification_checkbox).Click();
                    Thread.Sleep(3000);
                    ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].scrollIntoView(true);", Driver.FindElement(BtnNext));
                    Thread.Sleep(2000);
                    wait.Until(x => x.FindElement(BtnNext));
                    //Waiting for the element to be visible and clicking on next button
                    Driver.FindElement(BtnNext).Click();
                    wait.Until(x => x.FindElement(NumGroups_Element));
                    Driver.FindElement(NumGroups_Element).SendKeys(row.ItemArray[1].ToString());
                    //wait.Until(x => x.FindElement(Teachershare_dropdown));
                    //SelectElement Teachershare = new SelectElement(Driver.FindElement(Teachershare_dropdown));
                    //Teachershare.SelectByText(row.ItemArray[2].ToString());
                    //SelectElement Awardagreement = new SelectElement(Awardagreement_dropdown);
                    //Awardagreement.SelectByText(row.ItemArray[3].ToString());
                    //SelectElement TeacherLevel = new SelectElement(TeacherLevel_dropdown);
                    //TeacherLevel.SelectByText(row.ItemArray[4].ToString());
                    wait.Until(x => x.FindElement(NumHours_Element));
                    Driver.FindElement(NumHours_Element).SendKeys(row.ItemArray[5].ToString());
                    wait.Until(x => x.FindElement(SaveBtn));
                    Thread.Sleep(2000);
                    Driver.FindElement(SaveBtn).Click();
                    Thread.Sleep(2000);
                }
            }

        }
        internal void MakeInactiveTeacherActive()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(50));
            wait.Until(x => x.FindElement(TeachersTab_Element));
            Driver.FindElement(TeachersTab_Element).Click();

            wait.Until(x => x.FindElement(ShowInactiveTeacherButton_Teacher_Element));
            Driver.FindElement(ShowInactiveTeacherButton_Teacher_Element).Click();
            char[] charsToTrim = { 'e', 'n', 't', 'r', 'i', 's' };
            String strTeachersTabUntrimmedNoofInactiveTeachers = Driver.FindElement(TeachersTabNoofInactiveTeacherElement).Text;
            String strFirstTrimNoofInactiveTeachers = strTeachersTabUntrimmedNoofInactiveTeachers.TrimEnd(charsToTrim);
            String strSecondTrimNoofInactiveTeachers = strFirstTrimNoofInactiveTeachers.Substring(strFirstTrimNoofInactiveTeachers.IndexOf("of") + 2);
            String strNoofInactiveTeachers = strSecondTrimNoofInactiveTeachers.Trim();
            if (strNoofInactiveTeachers.Equals("0"))
            {

            }
            else
            {
                IList<IWebElement> TableRows = Driver.FindElements(By.XPath("//tbody[@id='TeacherSummaryDataGrid']//tr"));
                for (int i = 1; i <= TableRows.Count; i++)
                {
                    wait.Until(x => x.FindElement(By.XPath("//tbody[@id='TeacherSummaryDataGrid']//tr[1]//button[@class='btn blue activate']")).Displayed);
                    Driver.FindElement(By.XPath("//tbody[@id='TeacherSummaryDataGrid']//tr[1]//button[@class='btn blue activate']")).Click();
                    Thread.Sleep(2000);
                }
            };

            wait.Until(x => x.FindElement(ShowActiveTeacherButton_Teacher_Element));
            Driver.FindElement(ShowActiveTeacherButton_Teacher_Element).Click();
        }
        public int NoofTeacher()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(160));
            //Checking value for number of teachers in Teachers Tab as the expected result
            wait.Until(x => x.FindElement(By.XPath("//strong[contains(text(),'Teachers')]")));
            Driver.FindElement(By.XPath("//strong[contains(text(),'Teachers')]")).Click();

            Thread.Sleep(2000);

            char[] charsToTrim = { 'e', 'n', 't', 'r', 'i', 's' };
            String strTeachersTabUntrimmedNoofTeachers = Driver.FindElement(By.CssSelector("#DataTables_Table_0_info")).Text;
            String strFirstTrimNoofTeachers = strTeachersTabUntrimmedNoofTeachers.TrimEnd(charsToTrim);
            String strSecondTrimNoofTeachers = strFirstTrimNoofTeachers.Substring(strFirstTrimNoofTeachers.IndexOf("of") + 2);
            String strNoofTeachers = strSecondTrimNoofTeachers.Trim();

            int noofTeacher = Convert.ToInt32(strNoofTeachers);
            return noofTeacher;
        }

    }
}