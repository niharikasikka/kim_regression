﻿using KIM_SmokeTest.BaseClass;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;


namespace KIM_Automation.Pages
{
    class AdjustmentsPage : BaseApplicationPage
    {
        public AdjustmentsPage(IWebDriver driver) : base(driver)
        { }

        //Adjustments Elements
        public By AdjustmentsTabElement => By.XPath("//strong[contains(text(),'Adjustments')]");
        public By AdjustmentsTabStatusElement => By.XPath("//span[@class='status adjust-tab-status']");
        public By AdjustmentsHeaderElement => By.XPath("//div[@id='tab6']//h2[contains(text(),'Adjustments')]");
        public By AdjustmentsTextElement => By.XPath("//p[contains(text(),'Summary of enrolment information for the funded ki')]");
        public By Adj_ConfirmedEnrolments_Element => By.XPath("//label[contains(text(),'Confirmed Enrolments')]");
        public By Adj_ConfirmedEnrolments_Textbox_Element => By.CssSelector("#adjCnEnrolments");
        public By Adj_ConfirmedKFS_Element => By.XPath("//label[contains(text(),'Confirmed Kindergarten Fee Subsidy')]");
        public By Adj_ConfirmedKFS_Textbox_Element => By.CssSelector("#adjCnKFSEnrolments");
        public By Adj_TotalEnrolments_Element => By.XPath("//label[contains(text(),'Total Enrolments')]");
        public By Adj_TotalEnrolments_Textbox_Element => By.CssSelector("#adjTotalEnrolments");
        public By Adj_TotalKFS_Element => By.XPath("//label[contains(text(),'Total Kindergarten Fee Subsidy')]");
        public By Adj_TotalKFS_Textbox_Element => By.CssSelector("#adjTotalKFSEnrolments");

        //Program Tab Elements to be used for Adjustment Tab
        public By ProgramsTabElement => By.XPath("//strong[contains(text(),'Programs')]");
        public By ProgramTabStatusElement => By.XPath("//span[@class='status program-tab-status']");


        //Teachers Tab Elements to be used for Adjustment Tab
        public By TeachersTabElement => By.XPath("//strong[contains(text(),'Teachers')]");
        public By TeachersTabStatusElement => By.XPath("//span[@class='status teacher-tab-status']");


        //Other Educators Tab Elements to be used for Adjustment Tab
        public By OtherEducatorsTabElement => By.XPath("//strong[contains(text(),'Other Educators')]");
        public By OtherEducatorsTabStatusElement => By.XPath("//span[@class='status assistant-tab-status']");


        //Enrolments Tab Elements to be used for Adjustment Tab
        public By EnrolmentsTabElement => By.CssSelector("a[href='#tab4']");
        public By EnrolmentsTabStatusElement => By.XPath("//span[@class='status enrolment-tab-status']");
        public By EnrolmentsTabNoofEnrolmentsElement => By.CssSelector("#DataTables_Table_2_info");

        //Annual Confirmation Tab Elements to be used for Adjustment Tab
        public By AnnualConfirmationTabElement => By.CssSelector("#profileTabsNav > li:nth-child(5) > a");
        public By AnnualConfirmationTabStatusElement = By.XPath("//span[@class='status confirm-tab-status']");

        internal void Adjustments()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(160));
            wait.Until(x => x.FindElement(AdjustmentsTabElement));
            Driver.FindElement(AdjustmentsTabElement).Click();
            //AnnualConfirmationTabElement.Click();
        }
        internal void AdjustmentsAlert()
        {
            Thread.Sleep(2000);
            IAlert AdjustmentsAlert = Driver.SwitchTo().Alert();
            //aCAlert.Text();
            AdjustmentsAlert.Accept();
            Thread.Sleep(2000);
        }
        internal void CheckTEPEAStatus()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(160));
            //check the Teachers tab status
            wait.Until(x => x.FindElement(TeachersTabStatusElement));
            String strTeacherTabStatus = Driver.FindElement(TeachersTabStatusElement).Text;

            if (strTeacherTabStatus.Equals("Read only"))
            {
                //throw new Exception("PASS: Teacher tab status is " + strTeacherTabStatus);
            }
            else
            {
                throw new Exception("FAIL: Teacher tab status is " + strTeacherTabStatus);
            };

            //check the Other Educators tab status
            wait.Until(x => x.FindElement(OtherEducatorsTabStatusElement));
            String strOtherEducatorsTabStatus = Driver.FindElement(OtherEducatorsTabStatusElement).Text;
            if (strOtherEducatorsTabStatus.Equals("Read only"))
            {
                //throw new Exception("PASS: Other Educators tab status is " + strOtherEducatorsTabStatus);
            }
            else
            {
                throw new Exception("FAIL: Other Educators tab status is " + strOtherEducatorsTabStatus);
            };

            //check the Programs tab status
            wait.Until(x => x.FindElement(ProgramTabStatusElement));
            String strProgramTabStatus = Driver.FindElement(ProgramTabStatusElement).Text;
            if (strProgramTabStatus.Equals("Read only"))
            {
                //throw new Exception("PASS: Programs tab status is " + strProgramTabStatus);
            }
            else
            {
                throw new Exception("FAIL: Programs tab status is " + strProgramTabStatus);
            };

            //check the Enrolments tab status
            wait.Until(x => x.FindElement(EnrolmentsTabStatusElement));
            String strEnrolmentsTabStatus = Driver.FindElement(EnrolmentsTabStatusElement).Text;
            if (strEnrolmentsTabStatus.Equals("Read only"))
            {
                //throw new Exception("PASS: Enrolments tab status is " + strEnrolmentsTabStatus);
            }
            else
            {
                throw new Exception("FAIL: Enrolments tab status is " + strEnrolmentsTabStatus);
            };

            //check the Annual Confirmation tab status
            wait.Until(x => x.FindElement(AnnualConfirmationTabStatusElement));
            String strAnnualConfirmationTabStatus = Driver.FindElement(AnnualConfirmationTabStatusElement).Text;
            if (strAnnualConfirmationTabStatus.Equals("Submitted"))
            {
                //throw new Exception("PASS: Annual Confirmation tab status is " + strAnnualConfirmationTabStatus);
            }
            else
            {
                throw new Exception("FAIL: Annual Confirmation tab status is " + strAnnualConfirmationTabStatus);
            };
        }
        internal void CheckTEPEAStatusAfterApprove()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(160));
            //check the Teachers tab status
            wait.Until(x => x.FindElement(TeachersTabStatusElement));
            String strTeacherTabStatus = Driver.FindElement(TeachersTabStatusElement).Text;

            if (strTeacherTabStatus.Equals("Complete"))
            {
                //throw new Exception("PASS: Teacher tab status is " + strTeacherTabStatus);
            }
            else
            {
                throw new Exception("FAIL: Teacher tab status is " + strTeacherTabStatus);
            };

            //check the Other Educators tab status
            wait.Until(x => x.FindElement(OtherEducatorsTabStatusElement));
            String strOtherEducatorsTabStatus = Driver.FindElement(OtherEducatorsTabStatusElement).Text;
            if (strOtherEducatorsTabStatus.Equals("Complete"))
            {
                //throw new Exception("PASS: Other Educators tab status is " + strOtherEducatorsTabStatus);
            }
            else
            {
                throw new Exception("FAIL: Other Educators tab status is " + strOtherEducatorsTabStatus);
            };

            //check the Programs tab status
            wait.Until(x => x.FindElement(ProgramTabStatusElement));
            String strProgramTabStatus = Driver.FindElement(ProgramTabStatusElement).Text;
            if (strProgramTabStatus.Equals("Complete"))
            {
                //throw new Exception("PASS: Programs tab status is " + strProgramTabStatus);
            }
            else
            {
                throw new Exception("FAIL: Programs tab status is " + strProgramTabStatus);
            };

            //check the Enrolments tab status
            wait.Until(x => x.FindElement(EnrolmentsTabStatusElement));
            String strEnrolmentsTabStatus = Driver.FindElement(EnrolmentsTabStatusElement).Text;
            if (strEnrolmentsTabStatus.Equals("Complete"))
            {
                //throw new Exception("PASS: Enrolments tab status is " + strEnrolmentsTabStatus);
            }
            else
            {
                throw new Exception("FAIL: Enrolments tab status is " + strEnrolmentsTabStatus);
            };

            //check the Annual Confirmation tab status
            wait.Until(x => x.FindElement(AnnualConfirmationTabStatusElement));
            String strAnnualConfirmationTabStatus = Driver.FindElement(AnnualConfirmationTabStatusElement).Text;
            if (strAnnualConfirmationTabStatus.Equals("Complete"))
            {
                //throw new Exception("PASS: Annual Confirmation tab status is " + strAnnualConfirmationTabStatus);
            }
            else
            {
                throw new Exception("FAIL: Annual Confirmation tab status is " + strAnnualConfirmationTabStatus);
            };
        }
        internal void AdjustmentsTabStatus()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(160));
            //check the Teachers tab status
            wait.Until(x => x.FindElement(AdjustmentsTabStatusElement));
            String strAdjustmentsTabStatus = Driver.FindElement(AdjustmentsTabStatusElement).Text;

            if (strAdjustmentsTabStatus.Equals("Available"))
            {
                //throw new Exception("PASS: Adjustments tab status is " + strAdjustmentsTabStatus);
            }
            else
            {
                throw new Exception("FAIL: Adjustments tab status is " + strAdjustmentsTabStatus);
            };
        }
        internal void AdjustmentsTabAvailableFields()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(160));
            //check available fields
            //check the Adjustment header
            wait.Until(x => x.FindElement(AdjustmentsHeaderElement));
            Thread.Sleep(5000);
            String strAdjHeader = Driver.FindElement(AdjustmentsHeaderElement).Text;
            if (strAdjHeader.Equals("Adjustments"))
            {

            }
            else
            {
                throw new Exception("FAIL: Adjustments header is " + strAdjHeader);
            }
            //check the Adjustment text
            wait.Until(x => x.FindElement(AdjustmentsTextElement));
            String strAdjText = Driver.FindElement(AdjustmentsTextElement).Text;
            if (strAdjText.Equals("Summary of enrolment information for the funded kindergarten program in the year before school"))
            {

            }
            else
            {
                throw new Exception("FAIL: Adjustments text is " + strAdjText);
            }
            //check the Confirmed Enrolments
            wait.Until(x => x.FindElement(Adj_ConfirmedEnrolments_Element));
            String strAdjConfirmedEnrolments = Driver.FindElement(Adj_ConfirmedEnrolments_Element).Text;
            if (strAdjConfirmedEnrolments.Equals("Confirmed Enrolments"))
            {

            }
            else
            {
                throw new Exception("FAIL: Confirmed Enrolments is labeled as " + strAdjConfirmedEnrolments);
            }
            wait.Until(x => x.FindElement(Adj_ConfirmedEnrolments_Textbox_Element));
            Driver.FindElement(Adj_ConfirmedEnrolments_Textbox_Element);

            //check the Confirmed KFS
            wait.Until(x => x.FindElement(Adj_ConfirmedKFS_Element));
            String strAdjConfirmedKFS = Driver.FindElement(Adj_ConfirmedKFS_Element).Text;
            if (strAdjConfirmedKFS.Equals("Confirmed Kindergarten Fee Subsidy"))
            {

            }
            else
            {
                throw new Exception("FAIL: Confirmed Kindergarten Fee Subsidy is labeled as " + strAdjConfirmedKFS);
            }
            wait.Until(x => x.FindElement(Adj_ConfirmedKFS_Textbox_Element));
            Driver.FindElement(Adj_ConfirmedKFS_Textbox_Element);

            // check the Total Enrolments
            wait.Until(x => x.FindElement(Adj_TotalEnrolments_Element));
            String strAdjTotalEnrolments = Driver.FindElement(Adj_TotalEnrolments_Element).Text;
            if (strAdjTotalEnrolments.Equals("Total Enrolments"))
            {

            }
            else
            {
                throw new Exception("FAIL: Total Enrolments is labeled as " + strAdjTotalEnrolments);
            }
            wait.Until(x => x.FindElement(Adj_TotalEnrolments_Textbox_Element));
            Driver.FindElement(Adj_TotalEnrolments_Textbox_Element);

            //check the Total KFS
            wait.Until(x => x.FindElement(Adj_TotalKFS_Element));
            String strAdjTotalKFS = Driver.FindElement(Adj_TotalKFS_Element).Text;
            if (strAdjTotalKFS.Equals("Total Kindergarten Fee Subsidy"))
            {

            }
            else
            {
                throw new Exception("FAIL: Total Kindergarten Fee Subsidy is labeled as " + strAdjTotalKFS);
            }
            wait.Until(x => x.FindElement(Adj_TotalKFS_Textbox_Element));
            Driver.FindElement(Adj_TotalKFS_Textbox_Element);


        }
        internal void VerifyValuesAdjustmentsTab()
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(160));
            
            //Checking value for number of Enrolments in Enrolments Tab as the expected result
            //Thread.Sleep(8000);
            wait.Until(x => x.FindElement(EnrolmentsTabElement));
            Driver.FindElement(EnrolmentsTabElement).Click();

            char[] charsToTrim = { 'e', 'n', 't', 'r', 'i', 's' };
            wait.Until(x => x.FindElement(EnrolmentsTabNoofEnrolmentsElement));
            String strEnrolmentsTabUntrimmedNoofEnrolments = Driver.FindElement(EnrolmentsTabNoofEnrolmentsElement).Text;
            //throw new Exception(strEnrolmentsTabUntrimmedNoofEnrolments);
            String strFirstTrimNoofEnrolments = strEnrolmentsTabUntrimmedNoofEnrolments.TrimEnd(charsToTrim);
            String strSecondTrimNoofEnrolments = strFirstTrimNoofEnrolments.Substring(strFirstTrimNoofEnrolments.IndexOf("of") + 2);
            String strNoofEnrolments = strSecondTrimNoofEnrolments.Trim();
            wait.Until(x => x.FindElement(Adj_TotalEnrolments_Textbox_Element));
            String strARTotalEnrolments = Driver.FindElement(Adj_TotalEnrolments_Textbox_Element).GetAttribute("value");
            String strARTotalKFS = Driver.FindElement(Adj_TotalKFS_Textbox_Element).GetAttribute("value");
            int erTotalEnrolments = Int32.Parse(strNoofEnrolments);
            int arTotalKFS = Int32.Parse(strARTotalKFS);
            int nonKFS = Driver.FindElements(By.XPath("//tbody[@id='EnrolmentSummaryDataGrid']//td[contains(text(),'No')]")).Count;
            int kFS = erTotalEnrolments - nonKFS;

            
            //Checking the actual values in Adjustments tab and comparing it the expected results

            wait.Until(x => x.FindElement(AdjustmentsTabElement));
            Driver.FindElement(AdjustmentsTabElement).Click();

            // check the Confirmed Enrolments vs Expected Result
            // ---> still checking how to verify value in Confirmed Enrolments

            // check the Confirmed KFS vs Expected Result
            // ---> still checking how to verify value in Confirmed KFS


            // check the Total Enrolments vs Expected Result
            if (strARTotalEnrolments.Equals(strNoofEnrolments))
            {

            }
            else
            {
                throw new Exception("FAIL - Adjustments' value for Total Enrolments: " + strARTotalEnrolments + " is not the same as Enrolments Tab's value: " + strNoofEnrolments);
            };
            
            // check the Total KFS vs Expected Result
            if (arTotalKFS.Equals(kFS))
            {

            }
            else
            {
                throw new Exception("FAIL - Adjustments' value for Total KFS: " + arTotalKFS + " is not the same as Enrolments Tab's value: " + kFS);
            };
            
        }

    }
}
